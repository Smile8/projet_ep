function [ X, t, tx_perte, tx_occupation, debit, moyenRqt ] = CMTC( M, T, P0, txArrivee )
    % P0 loi empirique associ� � l'�tat initial
    % T dur�e d'observation
    % M matrice des taux de transition
    n=size(M,1);
    % D�terminer Q
    Q=[];
    for i=1:n
        Q(i,:)=-M(i,:)/M(i,i);
        Q(i,i)=0;
    end
    i=1;
    rqt=0;
    etat_inocc=0;
    rqt_s=0;
    moyenRqt=0;
    totalRqt=0;
    nbEtat = length(P0);
    X(1)=loi_empirique(P0, [1:n]); % G�n�rer l'�tat initial
    S(1)=loi_exponentiel_1(-M(X(1),X(1))); % Temps de s�jour de l'�tat X(1)
    t(1)=S(1);
    while(t(i)<T)
        X(i+1)=loi_empirique(Q(X(i),:), [1:n]);
        S(i+1)=loi_exponentiel_1(-M(X(i+1), X(i+1)));
        t(i+1)=t(i)+S(i+1);
        
        %if X(i)==1 && X(i+1)==2
         %   rqt=rqt+1;
        %end
        moyenRqt=moyenRqt+(X(i)-1);
        
        totalRqt=totalRqt+1;
        
        for j=1:nbEtat-1
            if X(i)==j && X(i+1)==j+1
                rqt=rqt+1;
            end
        end
         
        for j=1:nbEtat-1
            if X(i)==j && X(i+1)==j+1
                rqt_s=rqt_s+1;
            end
        end
        
        for j=1:nbEtat-1
            if X(i+1)==j && X(i)==j+1
                rqt_s=rqt_s+1;
            end
        end
        
        if X(i)==1
            etat_inocc=etat_inocc+S(i);
        end
        
        i=i+1;
    end

    tx_occupation=1-etat_inocc/T; % 33,33 %
    tx_perte=1-(rqt/(T*txArrivee)); % 33,33 %

    %tx_perte=((1-(lambda/mu))/(1-(lambda/mu)^nbEtat))*(lambda/mu)^(nbEtat-1);
    debit=rqt_s/T;
    moyenRqt=moyenRqt/totalRqt;
    % D�bit = 555,57 R/S
    % Temps moyen de s�jour R=1/2000(s)
   % stairs(t,X);
end

