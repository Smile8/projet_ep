function [ X, t, tx_perte, tx_occupation, debit, Q ] = CMTC( M, T, P0, txArrivee )
    % P0 loi empirique associ� � l'�tat initial
    % T dur�e d'observation
    % M matrice des taux de transition
    n=size(M,1);
    % D�terminer Q
    Q=[];
    for i=1:n
        Q(i,:)=-M(i,:)/M(i,i);
        Q(i,i)=0;
    end
    i=1;
    rqt=0;
    etat_occ=0;
    rqt_s=0;
    X(1)=loi_empirique(P0, [1:n]); % G�n�rer l'�tat initial
    S(1)=loi_exponentiel_1(-M(X(1),X(1))); % Temps de s�jour de l'�tat X(1)
    t(1)=S(1);
    while(t(i)<T)
        X(i+1)=loi_empirique(Q(X(i),:), [1:n]);
        S(i+1)=loi_exponentiel_1(-M(X(i+1), X(i+1)));
        t(i+1)=t(i)+S(i+1);
        
        if X(i)==1 && X(i+1)==2 
            rqt=rqt+1;
        end
        
        if X(i+1)==1 && X(i)==2
            rqt_s=rqt_s+1;
        end
        
        if X(i)==2
            etat_occ=etat_occ+S(i);
        end
        
        i=i+1;
    end
    tx_occupation=etat_occ/T; % 33,33 %
    tx_perte=1-(rqt/(T*txArrivee)); % 33,33 %
    debit=rqt_s/T;
    % D�bit = 555,57 R/S
    % Temps moyen de s�jour R=1/2000(s)
    % stairs(t,X);
end

