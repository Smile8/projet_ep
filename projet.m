% -------------- Q.1 -------------- 
% TTraitement
load('TTraitement');
lambda=1/expfit(X);
Y=loi_exponentiel(1000, lambda);
T=kstest2(X,Y);  % Validation de la loi exponentielle pour TTraitement

% V�rification du Taux de traitement qui est �gal � Lambda
tpsTraitment=0;
for i=1:1000
    tpsTraitment=X(i)+tpsTraitment;
end

%Tx traitement
txTraitement=1000/tpsTraitment;

%Tps traitement
tpsTraitmentMoyen=tpsTraitment/1000;


% TArrivee
load('TArrivee.mat');
lambda=1/expfit(X);
Y=loi_exponentiel(1000, lambda);
A=kstest2(X,Y); % Validation de la loi exponentielle pour TArrivee

% V�rification du Taux d'arriv�e qui est �gal � Lambda
txArrivee=0;
for i=1:1000
    txArrivee=X(i)+txArrivee;
end
txArrivee=1000/txArrivee;

% -------------- Q.2 -------------- 

%Tx de traitement
mu=txTraitement;
P0=[1 0];
P1=[1 0 0];
P2=[1 0 0 0];
P3=[1 0 0 0 0];
P4=[1 0 0 0 0 0];
P5=[1 0 0 0 0 0 0];
P6=[1 0 0 0 0 0 0 0];
P7=[1 0 0 0 0 0 0 0 0];
P8=[1 0 0 0 0 0 0 0 0 0];
P9=[1 0 0 0 0 0 0 0 0 0 0];
P10=[1 0 0 0 0 0 0 0 0 0 0 0];

M=[-lambda lambda; mu -mu];
M1= [-lambda lambda 0 ; mu -(mu+lambda) lambda; 0 mu -mu ];
M2= [-lambda lambda 0 0 ;
     mu -(mu+lambda) lambda 0;
     0 mu -(mu+lambda) lambda;
     0 0 mu -mu];

 M3= [-lambda lambda 0 0 0;
     mu -(mu+lambda) lambda 0 0;
     0 mu -(mu+lambda) lambda 0;
     0 0 mu -(mu+lambda) lambda;
     0 0 0 mu -mu];

 M4= [-lambda lambda 0 0 0 0;
     mu -(mu+lambda) lambda 0 0 0;
     0 mu -(mu+lambda) lambda 0 0;
     0 0 mu -(mu+lambda) lambda 0;
     0 0 0 mu -(mu+lambda) lambda;
     0 0 0 0 mu -mu];

 M5= [-lambda lambda 0 0 0 0 0;
     mu -(mu+lambda) lambda 0 0 0 0;
     0 mu -(mu+lambda) lambda 0 0 0;
     0 0 mu -(mu+lambda) lambda 0 0;
     0 0 0 mu -(mu+lambda) lambda 0;
     0 0 0 0 mu -(mu+lambda) lambda;
     0 0 0 0 0 mu -mu];

 M6= [-lambda lambda 0 0 0 0 0 0;
     mu -(mu+lambda) lambda 0 0 0 0 0;
     0 mu -(mu+lambda) lambda 0 0 0 0;
     0 0 mu -(mu+lambda) lambda 0 0 0;
     0 0 0 mu -(mu+lambda) lambda 0 0;
     0 0 0 0 mu -(mu+lambda) lambda 0;
     0 0 0 0 0 mu -(mu+lambda) lambda;
     0 0 0 0 0 0 mu -mu];

 M7= [-lambda lambda 0 0 0 0 0 0 0;
     mu -(mu+lambda) lambda 0 0 0 0 0 0;
     0 mu -(mu+lambda) lambda 0 0 0 0 0;
     0 0 mu -(mu+lambda) lambda 0 0 0 0;
     0 0 0 mu -(mu+lambda) lambda 0 0 0;
     0 0 0 0 mu -(mu+lambda) lambda 0 0;
     0 0 0 0 0 mu -(mu+lambda) lambda 0;
     0 0 0 0 0 0 mu -(mu+lambda) lambda;
     0 0 0 0 0 0 0 mu -mu];

 M8= [-lambda lambda 0 0 0 0 0 0 0 0;
     mu -(mu+lambda) lambda 0 0 0 0 0 0 0;
     0 mu -(mu+lambda) lambda 0 0 0 0 0 0;
     0 0 mu -(mu+lambda) lambda 0 0 0 0 0;
     0 0 0 mu -(mu+lambda) lambda 0 0 0 0;
     0 0 0 0 mu -(mu+lambda) lambda 0 0 0;
     0 0 0 0 0 mu -(mu+lambda) lambda 0 0;
     0 0 0 0 0 0 mu -(mu+lambda) lambda 0;
     0 0 0 0 0 0 0 mu -(mu+lambda) lambda;
     0 0 0 0 0 0 0 0 mu -mu];

 M9= [-lambda lambda 0 0 0 0 0 0 0 0 0;
     mu -(mu+lambda) lambda 0 0 0 0 0 0 0 0;
     0 mu -(mu+lambda) lambda 0 0 0 0 0 0 0;
     0 0 mu -(mu+lambda) lambda 0 0 0 0 0 0;
     0 0 0 mu -(mu+lambda) lambda 0 0 0 0 0;
     0 0 0 0 mu -(mu+lambda) lambda 0 0 0 0;
     0 0 0 0 0 mu -(mu+lambda) lambda 0 0 0;
     0 0 0 0 0 0 mu -(mu+lambda) lambda 0 0;
     0 0 0 0 0 0 0 mu -(mu+lambda) lambda 0;
     0 0 0 0 0 0 0 0 mu -(mu+lambda) lambda;
     0 0 0 0 0 0 0 0 0 mu -mu];

 M10= [-lambda lambda 0 0 0 0 0 0 0 0 0 0;
     mu -(mu+lambda) lambda 0 0 0 0 0 0 0 0 0;
     0 mu -(mu+lambda) lambda 0 0 0 0 0 0 0 0;
     0 0 mu -(mu+lambda) lambda 0 0 0 0 0 0 0;
     0 0 0 mu -(mu+lambda) lambda 0 0 0 0 0 0;
     0 0 0 0 mu -(mu+lambda) lambda 0 0 0 0 0;
     0 0 0 0 0 mu -(mu+lambda) lambda 0 0 0 0;
     0 0 0 0 0 0 mu -(mu+lambda) lambda 0 0 0;
     0 0 0 0 0 0 0 mu -(mu+lambda) lambda 0 0;
     0 0 0 0 0 0 0 0 mu -(mu+lambda) lambda 0;
     0 0 0 0 0 0 0 0 0 mu -(mu+lambda) lambda;
     0 0 0 0 0 0 0 0 0 0 mu -mu];
 
%Evolution du nombre 
[ X, t, txPerte, txOccupation, debit ] = CMTC(M, 60, P0, txArrivee);
[ X1, t1, txPerte1, txOccupation1, debit1, moyenneRqt1 ] = CMTC_File(M1, 60, P1, txArrivee);
[ X2, t2, txPerte2, txOccupation2, debit2, moyenneRqt2 ] = CMTC_File(M2, 60, P2, txArrivee);
[ X3, t3, txPerte3, txOccupation3, debit3, moyenneRqt3 ] = CMTC_File(M3, 60, P3, txArrivee);
[ X4, t4, txPerte4, txOccupation4, debit4, moyenneRqt4 ] = CMTC_File(M4, 60, P4, txArrivee);
[ X5, t5, txPerte5, txOccupation5, debit5, moyenneRqt5 ] = CMTC_File(M5, 60, P5, txArrivee);
[ X6, t6, txPerte6, txOccupation6, debit6, moyenneRqt6 ] = CMTC_File(M6, 60, P6, txArrivee);
[ X7, t7, txPerte7, txOccupation7, debit7, moyenneRqt7 ] = CMTC_File(M7, 60, P7, txArrivee);
[ X8, t8, txPerte8, txOccupation8, debit8, moyenneRqt8 ] = CMTC_File(M8, 60, P8, txArrivee);
[ X9, t9, txPerte9, txOccupation9, debit9, moyenneRqt9 ] = CMTC_File(M9, 60, P9, txArrivee);
[ X10, t10, txPerte10, txOccupation10, debit10, moyenneRqt10 ] = CMTC_File(M10, 600, P10, txArrivee);

%Tps moyen de sejour
tpsMoyenSejour = debit/mu;
tpsMoyenSejour1 = debit1/mu;
tpsMoyenSejour2 = debit2/mu;
tpsMoyenSejour3 = debit3/mu;
tpsMoyenSejour4 = debit4/mu;
tpsMoyenSejour5 = debit5/mu;
tpsMoyenSejour6 = debit6/mu;
tpsMoyenSejour7 = debit7/mu;
tpsMoyenSejour8 = debit8/mu;
tpsMoyenSejour9 = debit9/mu;
tpsMoyenSejour10 = debit10/mu;

dataPerte=[txPerte txPerte1 txPerte2 txPerte3 txPerte4 txPerte5 txPerte6 txPerte7 txPerte8 txPerte9 txPerte10];
dataOccupation=[txOccupation txOccupation1 txOccupation2 txOccupation3 txOccupation4 txOccupation5 txOccupation6 txOccupation7 txOccupation8 txOccupation9 txOccupation10];
dataMoyenneRequete=[moyenneRqt1 moyenneRqt2 moyenneRqt3 moyenneRqt4 moyenneRqt5 moyenneRqt6 moyenneRqt7 moyenneRqt8 moyenneRqt9 moyenneRqt10];
dataDebit=[debit debit1 debit2 debit3 debit4 debit5 debit6 debit7 debit8 debit9 debit10];
dataTpsMoyenSejour=[tpsMoyenSejour tpsMoyenSejour1 tpsMoyenSejour2 tpsMoyenSejour3 tpsMoyenSejour4 tpsMoyenSejour5 tpsMoyenSejour6 tpsMoyenSejour7 tpsMoyenSejour8 tpsMoyenSejour9 tpsMoyenSejour10];
rqt=[];
rqt1=[];
for i=1:length(dataPerte)
    rqt(i)=i-1;
end
for i=1:length(dataMoyenneRequete)
    rqt1(i)=i;
end
figure(1);
plot(rqt, dataPerte, '--gs', 'MarkerEdgeColor', 'b');
title('Evolution du taux de perte');
xlabel('Taille de la liste d attente');
ylabel('Taux de perte');

figure(2);
plot(rqt, dataOccupation, '--gs', 'MarkerEdgeColor', 'b');
title('Evolution du taux d occupation');
xlabel('Taille de la liste d attente');
ylabel('Taux d occupation');

figure(3);
plot(rqt1, dataMoyenneRequete, '--gs', 'MarkerEdgeColor', 'b');
title('Evolution du nombre moyen de requetes dans le systeme');
xlabel('Taille de la liste d attente');
ylabel('Nombre moyen de requetes dans le systeme');

figure(4);
plot(rqt, dataDebit, '--gs', 'MarkerEdgeColor', 'b');
title('Evolution du debit');
xlabel('Taille de la liste d attente');
ylabel('Debit');

% -------------- Q.3 -------------- 







% -------------- Q.4 -------------- 

mu4000= 4000;
P4000=[1 0];
M4000=[-lambda lambda; mu4000 -mu4000];
[ X4000, t4000, txPerte4000, txOccupation4000, debit4000 ] = CMTC(M4000, 60, P4000, txArrivee);
tpsMoyenSejour4000 = debit4000/mu;
txRqtRejete= txPerte4000*100;

% -------------- Q.5 --------------
mu2Serv=2000;
P2Serv=[1 0 0];
M2Serv=[-lambda lambda 0 ; 
    mu2Serv -(mu2Serv+lambda) lambda; 
    0 2*mu2Serv -2*mu2Serv ];
[ X2Serv, t2Serv, txPerte2Serv, txOccupation2Serv, debit2Serv ] = CMTC_File(M2Serv, 60, P2Serv, txArrivee);

tpsMoyenSejour2Serv = debit2Serv/mu;
txRqtRejete2Serv= txPerte2Serv*100;

% -------------- Q.6 --------------

mu4Serv=1000;
P4Serv=[1 0 0 0 0];
M4Serv=[-lambda lambda 0 0 0;
     mu4Serv -(mu4Serv+lambda) lambda 0 0;
     0 mu4Serv -(mu4Serv+lambda) lambda 0;
     0 0 mu4Serv -(mu4Serv+lambda) lambda;
     0 0 0 4*mu4Serv -4*mu4Serv];
[ X4Serv, t4Serv, txPerte4Serv, txOccupation4Serv, debit4Serv ] = CMTC_File(M4Serv, 60, P4Serv, txArrivee);

tpsMoyenSejour4Serv = debit4Serv/mu;
txRqtRejete4Serv= txPerte4Serv*100;

data = [txRqtRejete txRqtRejete2Serv txRqtRejete4Serv];
%C = categorical(data,[txRqtRejete txRqtRejete2Serv txRqtRejete4Serv],{'1 serveur','2 serveurs','4 serveurs'})
figure(5);
bar(data);
title('Pourcentage de requ�tes rejet�s');
xlabel('1 = 1 serveur � 4000, 2 = 2 serveurs � 2000, 4 serveurs � 1000');
ylabel('Pourcentage de requ�tes rejet�s');

%plot(data,'r')