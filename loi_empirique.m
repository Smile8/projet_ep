function [ X ] = loi_empirique( P, Y )
    F=[]; % Init du vecteur F
    n=size(P,2); % Taille du vecteur P
    F(1)=P(1);
    for i=2:n
        F(i)=F(i-1)+P(i);
    end 
    u=rand; % V.A. uniformément distribuée (0,1)
    index=[];
    index=find(F>u);
    X=Y(index(1)); % X={1,2,...,n}
end

